CONNECTIVITY_KERNEL = [[1, 1, 1],
                       [1, 6, 1],
                       [1, 1, 1]]  # to detect neighbouring numbers

CENTER_POINT_MATRIX = [[0, 0, 0],
                       [0, 6, 0],
                       [0, 0, 0]]  # to remove central point from calculations

CONNECTIVITY_KERNEL_5 = [[1, 1, 1, 1, 1],
                        [1, 0, 0, 0, 1],
                        [1, 0, 6, 0, 1],
                        [1, 0, 0, 0, 1],
                        [1, 1, 1, 1, 1]]  # to detect neighbouring numbers

CENTER_POINT_MATRIX_5 = [[0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0],
                        [0, 0, 6, 0, 0],
                        [0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0]]  # to remove central point from calculations