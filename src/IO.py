import matplotlib.pyplot as pyplot
from matplotlib import colormaps
import numpy
import os
import statistics
from random import randint


def create_analysed_image(
        image_size,
        lines,
        image,
        image_file_name,
        analysis_folder_name,
):
    [image_height, image_width] = image_size
    fig = pyplot.figure(
        figsize=(image_width/128, image_height/128), dpi=128, frameon=False)
    pyplot.axis('off')
    pyplot.tight_layout()
    marker_size = max(int(max(image_height, image_width)/1000), 1)
    for instance_index in range(len(lines)):
        line = numpy.array([[point.y, point.x]
                            for point in lines[instance_index]])
        x, y = line.T
        cmap = colormaps['hsv']
        color = cmap(randint(1, 255))
        ax = pyplot.scatter(
            y, x, marker='.', s=marker_size, alpha=1, color=color)
        pyplot.annotate(
            str(instance_index + 1),
            (lines[instance_index][int(len(lines[instance_index])/3)].x,
                lines[instance_index][int(len(lines[instance_index])/3)].y),
            size=int(
                10+len(lines[instance_index])/50)
        )
    pyplot.xlim([0, image_width])
    pyplot.ylim([0, image_height])
    pyplot.gca().invert_yaxis()
    pyplot.imshow(image, alpha=1, cmap='Greys')
    pyplot.savefig(
        os.path.join(f'{analysis_folder_name}',
                     f'{image_file_name}.PNG'),
        transparent=True,
        bbox_inches=None,
        pad_inches=0.0
    )
    pyplot.close()


def objects_length_image(
        analysis_folder_name,
        image_file_name,
        lines_skeletonized,
        scale_bar,
):
    objects_lengths_in_image = []
    with open(
        os.path.join(
            analysis_folder_name,
            f'{image_file_name}_dimensions.txt'
        ),
            'w',
    ) as dimension_file:
        sum_length_of_objects = 0
        length_of_object = 0
        for instance_index in lines_skeletonized:
            sum_length_of_objects += length_of_object
            length_of_object = length_of_object_from_instance(
                instance_index,
                scale_bar,
            )
            objects_lengths_in_image.append(length_of_object)
            dimension_file.write(f'{length_of_object:.2f}\n')
    with open(
        os.path.join(
            analysis_folder_name,
            f'{image_file_name}_statistics.txt'),
            'w',
    ) as statistics_file:
        write_statistics(statistics_file, objects_lengths_in_image)
    return [objects_lengths_in_image, sum_length_of_objects]


def write_folder_statistics(analysis_folder_name, length_of_objects_in_images):
    with open(
        os.path.join(
            analysis_folder_name,
            'Statistics_of_objects_in_folder_images.txt',
        ),
            'w',
    ) as folder_statistics_file:
        write_statistics(folder_statistics_file, length_of_objects_in_images)


def length_of_object_from_instance(instance_index, scale_bar):
    length_of_object = 0
    for pixel in range(len(instance_index) - 1):
        length_of_object += numpy.sqrt(
            (
                instance_index[pixel + 1].y - instance_index[pixel].y
            ) ** 2 + (
                instance_index[pixel + 1].x - instance_index[pixel].x
            ) ** 2
        ) * scale_bar
    return length_of_object


def write_statistics(statistics_file, length_of_objects):
    standard_deviation = statistics.stdev(length_of_objects)
    statistics_file.write(f"""
Average length:\t\t\t\t\t{statistics.mean(length_of_objects):.2f}
Median:\t\t\t\t\t\t\t{numpy.median(length_of_objects):.2f}
Mode of the lengths:\t\t\t{statistics.mode(length_of_objects):.2f}
Variance of lengths:\t\t\t{statistics.variance(length_of_objects):.2f}
Standard deviation of lengths:\t{standard_deviation:.2f}
Error bar length:\t\t\t\t{
    standard_deviation/numpy.sqrt(len(length_of_objects))
    :.2f}
""")
