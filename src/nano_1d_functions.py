import os
import itertools
import numpy
import scipy
import skimage
from numpy import ndarray
from src.vector import Vector
from src.matrices import *
import cv2


class History:
    def __init__(self,  skeletonized_lines) -> None:
        self.skeletonized_lines = skeletonized_lines

    def scanned_count_by_shift(self, point, shift_x, shift_y):
        result = next((x for x in self.skeletonized_lines if x.x ==
                      point.x + shift_x and x.y == point.y + shift_y), 0)
        if type(result) == Vector:
            return result.scan_count
        else:
            return result

    def history_matrix_from_current_point(self, current_point):
        scanned_count_point = self.scanned_count_by_shift(current_point, 0, 0)
        scanned_count_W = self.scanned_count_by_shift(current_point, 1, 0)
        scanned_count_S = self.scanned_count_by_shift(current_point, 0, 1)
        scanned_count_SE = self.scanned_count_by_shift(current_point, 1, 1)
        scanned_count_E = self.scanned_count_by_shift(current_point, -1, 0)
        scanned_count_N = self.scanned_count_by_shift(current_point, 0, -1)
        scanned_count_NW = self.scanned_count_by_shift(current_point, -1, -1)
        scanned_count_SW = self.scanned_count_by_shift(current_point, -1, 1)
        scanned_count_NE = self.scanned_count_by_shift(current_point, 1, -1)
        result = [
            [scanned_count_NW, scanned_count_N, scanned_count_NE],
            [scanned_count_W, scanned_count_point, scanned_count_E],
            [scanned_count_SW, scanned_count_S, scanned_count_SE]
        ]
        return result


class ScoreMatrix:
    def __init__(self, center_slice) -> None:
        self.convoluted_slice = scipy.ndimage.convolve(
            center_slice,
            CONNECTIVITY_KERNEL,
            mode='constant',
            cval=0.0
        )
        self.center_slice = center_slice

    def from_tail(self):
        score_matrix = self.convoluted_slice - CENTER_POINT_MATRIX
        return score_matrix

    def from_section(self, compass_matrix):
        score_matrix = (self.convoluted_slice+compass_matrix +
                        compass_matrix+compass_matrix)*self.center_slice
        return score_matrix


class NextPoint:
    def __init__(self, current_point, skeletonized_image_binary) -> None:
        self.center_slice = neighbour_matrix_for_point(
            current_point, skeletonized_image_binary)
        self.current_point = current_point
        self.binary_image = skeletonized_image_binary

    def from_tail(self):
        score_matrix = ScoreMatrix(self.center_slice).from_tail()
        step_vector = next_step_vector_from_score_matrix(score_matrix)
        new_point = self.current_point.vector_by_adding_vector(
            step_vector)
        return [new_point, step_vector]

    def from_section(self, step_vector):
        compass_matrix = compass_matrix_from_last_step(
            step_vector)
        score_matrix = ScoreMatrix(
            self.center_slice).from_section(compass_matrix)
        step_vector = next_step_vector_from_score_matrix(score_matrix)
        new_point = self.current_point.vector_by_adding_vector(
            step_vector)
        return [new_point, step_vector]

    def from_intersection(
            self,
            current_line,
            search_distance,
            neighbour_number_image
    ):
        neighbour_matrix = neighbour_matrix_for_point(
            self.current_point, self.binary_image)
        travel_vector = travel_vector_for_line(current_line, search_distance)
        compass_matrix = compass_matrix_from_last_step(travel_vector)
        score_matrix = ScoreMatrix(
            neighbour_matrix).from_section(compass_matrix)
        intersection_point = self.current_point
        candidate_direction_lines: list[list[Vector[int]]] = [[], []]
        candidate_similarity_scores = []

        for candidate_index in [0, 1]:

            # Obtain the step direction vector based on the score matrix
            current_step_vector = next_step_vector_from_score_matrix(
                score_matrix)

            # Move towards the candidate direction
            current_candidate_point = intersection_point.vector_by_adding_vector(
                current_step_vector)
            current_candidate_points = candidate_direction_lines[candidate_index]
            current_candidate_points.append(intersection_point)
            current_candidate_points.append(current_candidate_point)

            # Remove this direction from the score matrix so that the next candidate step will not
            # choose the same direction by setting the corresponding element to zero
            score_matrix[current_step_vector.y +
                         1, current_step_vector.x + 1] = 0

            # Follow the chosen direction for SEARCH_DISTANCE steps
            current_candidate_points = candidate_similarity_scores_by_appending_current_candidate_score(
                travel_vector,
                candidate_similarity_scores,
                current_candidate_point,
                current_candidate_points,
                search_distance,
                self.binary_image,
                current_step_vector,
                neighbour_number_image
            )

        try:
            winner = numpy.int_(
                numpy.where(
                    candidate_similarity_scores == numpy.max(
                        candidate_similarity_scores
                    )
                )[0][0]
            )
        except:
            winner = 1

        winner_point: Vector[int] = candidate_direction_lines[winner][1]
        step_vector = winner_point.vector_by_subtracting_vector(
            self.current_point)
        return [winner_point, step_vector]


class Image:
    def __init__(self, brightness_threshould, skeletonization_method) -> None:
        self.threshold = brightness_threshould
        self.skeletonization_method = skeletonization_method

    def prepare(
            self,
            image_filepath,
            pre_soften,
            crop_image,
            crop_height,
            sigma,
            save_skeletonized
    ):

        image_file_name: str = os.path.basename(image_filepath)
        folder_name = os.path.dirname(image_filepath)

        self.image: numpy._typing.ArrayLike = cv2.imread(
            image_filepath, cv2.IMREAD_GRAYSCALE)

        if crop_image:
            self.image = self.image[0:crop_height, :]

        if pre_soften:
            self.image = skimage.filters.gaussian(
                self.image,
                sigma=sigma,
                truncate=2.5,
                preserve_range=True,
                )

        # Skeletonize Image
        [skeletonized_image_boolean,
         binary_image,
         threshold,
         ] = self.skeletonized()

        if save_skeletonized:
            skimage.io.imsave(
                os.path.join(f'{folder_name}_analysed',
                             f'{image_file_name}_skeletonized.PNG'),
                skimage.util.img_as_ubyte(skeletonized_image_boolean),
                check_contrast=False
            )

        skeletonized_image_binary = 1 * skeletonized_image_boolean
        skeletonized_image_binary = cv2.copyMakeBorder(
            skeletonized_image_binary, 1, 1, 1, 1, cv2.BORDER_CONSTANT, 0)

        return [self.image, binary_image, skeletonized_image_binary, threshold]

    def skeletonized(self):
        if self.threshold == 0:
            self.threshold = int(
                skimage.filters.threshold_otsu(self.image))
        boolean_image = self.image > self.threshold
        binary_image = boolean_image * 1
        skeletonized_image_boolean = skimage.morphology.skeletonize(
            boolean_image, method=self.skeletonization_method)
        if self.skeletonization_method == 'lee':
            skeletonized_image_boolean = skeletonized_image_boolean / 255
        return [skeletonized_image_boolean, binary_image, self.threshold]


def neighbour_matrix_for_point(
        point: Vector[int],
        digitized_image: ndarray[int],
):
    return digitized_image[point.y - 1:point.y + 2, point.x - 1:point.x + 2]


def neighbour_matrix_for_point_5(
        point: Vector[int],
        digitized_image: ndarray[int],
):
    return digitized_image[point.y - 2:point.y + 3, point.x - 2:point.x + 3]


def next_step_vector_from_score_matrix(score_matrix):
    next_step = Vector(
        x=numpy.where(score_matrix == numpy.max(score_matrix))[1][0] - 1,
        y=numpy.where(score_matrix == numpy.max(score_matrix))[0][0] - 1
    )
    return next_step


def travel_vector_for_line(
        line_points: list[Vector[int]],
        search_distance: int,
):
    if len(line_points) >= search_distance:
        return Vector(
            x=line_points[-1].x - line_points[-1 * search_distance].x,
            y=line_points[-1].y - line_points[-1 * search_distance].y
        )

    return Vector(
        x=line_points[-1].x - line_points[0].x,
        y=line_points[-1].y - line_points[0].y
    )


def candidate_similarity_scores_by_appending_current_candidate_score(
        travel_vector,
        candidate_similarity_scores,
        current_candidate_point,
        current_candidate_points,
        search_distance,
        skeletonized_image_binary,
        current_step_vector,
        neighbour_counts_image
):
    for step in range(search_distance):

        neighbour_matrix = neighbour_matrix_for_point(
            current_candidate_point,
            skeletonized_image_binary
        )
        compass_matrix = compass_matrix_from_last_step(
            current_step_vector)
        candidate_score_matrix = ScoreMatrix(
            neighbour_matrix).from_section(compass_matrix)
        candidate_step_vector = next_step_vector_from_score_matrix(
            candidate_score_matrix)
        current_candidate_point = current_candidate_point.vector_by_adding_vector(
            candidate_step_vector
        )
        if neighbour_counts_image[
            current_candidate_point.y,
            current_candidate_point.x
        ] == 3:
            break
        if neighbour_counts_image[
            current_candidate_point.y,
            current_candidate_point.x
        ] == 1:
            current_candidate_points.append(
                current_candidate_point)
            break
        current_candidate_points.append(
            current_candidate_point)
    candidate_travel_vector = travel_vector_for_line(
        current_candidate_points,
        search_distance
    )
    candidate_similarity_score = travel_vector.unit_vector.dot_product_with_vector(
        candidate_travel_vector.unit_vector
    )
    candidate_similarity_scores.append(candidate_similarity_score)
    return candidate_similarity_scores


def remove_shared_tails(skeletonized_lines: list, search_distance: int):
    # Compare each line with the rest of lines
    for first_line, second_line in itertools.combinations(
        skeletonized_lines,
        2,
    ):
        # skips the current iteration of the loop and the control flow of the program goes to the next iteration.
        if not share_tail(first_line, second_line):
            continue

        if first_line in skeletonized_lines:
            skeletonized_lines.remove(first_line)
        if second_line in skeletonized_lines:
            skeletonized_lines.remove(second_line)

        # This reversing should be good for speed
        first_line.reverse()
        second_line.reverse()
        first_line_reversed = first_line
        second_line_reversed = second_line

        # Find the intersection
        intersection_point = first_line[0]
        for first_line_point in first_line_reversed:
            if first_line_point in second_line:
                intersection_point = first_line_point
                break

        shared_line = first_line_reversed[-1:first_line_reversed.index(
            intersection_point):-1]
        first_line = first_line_reversed[first_line_reversed.index(
            intersection_point)::-1]
        second_line = second_line_reversed[second_line_reversed.index(
            intersection_point)::-1]

        # firstLine[::-1] and secondLine[::-1] are reversed copies of the lines
        shared_line_travel_vector = travel_vector_for_line(
            shared_line, search_distance).unit_vector
        first_line_travel_vector = travel_vector_for_line(
            first_line[::-1],
            search_distance
        ).unit_vector.vector_by_scaling_vector(-1)
        second_line_travel_vector = travel_vector_for_line(
            second_line[::-1],
            search_distance
        ).unit_vector.vector_by_scaling_vector(-1)

        # Compare the travel vectors
        first_line_similarity = shared_line_travel_vector.dot_product_with_vector(
            first_line_travel_vector)
        second_line_similarity = shared_line_travel_vector.dot_product_with_vector(
            second_line_travel_vector
        )

        if second_line_similarity > first_line_similarity:
            skeletonized_lines.append(
                shared_line + second_line)
            skeletonized_lines.append(first_line)
        else:
            skeletonized_lines.append(
                shared_line + first_line)
            skeletonized_lines.append(second_line)


def share_tail(first_line: list, second_line: list):
    result = False
    if first_line[0] == second_line[0]:
        result = True
    if first_line[0] == second_line[-1]:
        second_line.reverse()
        result = True
    if first_line[-1] == second_line[-1]:
        first_line.reverse()
        second_line.reverse()
        result = True
    if first_line[-1] == second_line[0]:
        first_line.reverse()
        result = True
    return result


def shared_tails_count(skeletonized_lines: list):
    result = 0
    for line_pair in itertools.combinations(skeletonized_lines, 2):
        first_line: list[Vector[int]] = line_pair[0]
        second_line: list[Vector[int]] = line_pair[1]

        # skips the current iteration of the loop and the control flow of the program goes to the next iteration.
        if not share_tail(first_line, second_line):
            continue
        else:
            result += 1
    return result


def image_of_neighbour_counts(
        digitized_image: numpy.ndarray[int],
) -> numpy.ndarray[
        int]:
    connectivity_image = scipy.ndimage.convolve(
        digitized_image,
        CONNECTIVITY_KERNEL,
        mode='constant',
        cval=0.0
    )
    result = (
        connectivity_image * (connectivity_image > 6) - 6
    ) * (
        (connectivity_image * (connectivity_image > 6) - 6) > 0
    )
    return result


def points_by_value(
        value: int,
        matrix: numpy.ndarray[int],
) -> list[Vector[int]]:
    tail_point_coordinates_in_parallel_lists = numpy.where(
        matrix == value
    )  # a tuple of ([y1, y2, ...]), ([x1, x2, ...])
    NNs = len(tail_point_coordinates_in_parallel_lists[0]) * [value]
    tail_points = [
        Vector.yx_from_tuple(coordinates) for coordinates in zip(
            tail_point_coordinates_in_parallel_lists[0],
            tail_point_coordinates_in_parallel_lists[1],
            NNs,
        )
    ]
    return tail_points


def remove_four_way_intersection_points(
        four_way_intersection_points: list[Vector[int]],
        neighbour_counts_matrix: ndarray[int],
        digitized_image: ndarray[int]
):
    for intersection in four_way_intersection_points:
        if neighbour_counts_matrix[intersection.y + 1, intersection.x] == 3 and neighbour_counts_matrix[
                intersection.y + 1, intersection.x - 1] != 0 and neighbour_counts_matrix[
                intersection.y + 1, intersection.x + 1] != 0:
            neighbour_counts_matrix[intersection.y + 1, intersection.x] = 0
            neighbour_counts_matrix[intersection.y +
                                    1, intersection.x - 1] -= 1
            neighbour_counts_matrix[intersection.y +
                                    1, intersection.x + 1] -= 1
            neighbour_counts_matrix[intersection.y, intersection.x] = 3
            digitized_image[intersection.y + 1, intersection.x] = 0
        if neighbour_counts_matrix[intersection.y - 1, intersection.x] == 3 and neighbour_counts_matrix[
                intersection.y - 1, intersection.x - 1] != 0 and neighbour_counts_matrix[
                intersection.y - 1, intersection.x + 1] != 0:
            neighbour_counts_matrix[intersection.y - 1, intersection.x] = 0
            neighbour_counts_matrix[intersection.y -
                                    1, intersection.x - 1] -= 1
            neighbour_counts_matrix[intersection.y -
                                    1, intersection.x + 1] -= 1
            neighbour_counts_matrix[intersection.y, intersection.x] = 3
            digitized_image[intersection.y - 1, intersection.x] = 0
        if neighbour_counts_matrix[intersection.y, intersection.x - 1] == 3 and neighbour_counts_matrix[
                intersection.y - 1, intersection.x - 1] != 0 and neighbour_counts_matrix[
                intersection.y + 1, intersection.x - 1] != 0:
            neighbour_counts_matrix[intersection.y, intersection.x - 1] = 0
            neighbour_counts_matrix[intersection.y -
                                    1, intersection.x - 1] -= 1
            neighbour_counts_matrix[intersection.y +
                                    1, intersection.x - 1] -= 1
            neighbour_counts_matrix[intersection.y, intersection.x] = 3
            digitized_image[intersection.y, intersection.x - 1] = 0
        if neighbour_counts_matrix[intersection.y, intersection.x + 1] == 3 and neighbour_counts_matrix[
                intersection.y - 1, intersection.x + 1] != 0 and neighbour_counts_matrix[
                intersection.y + 1, intersection.x + 1] != 0:
            neighbour_counts_matrix[intersection.y, intersection.x + 1] = 0
            neighbour_counts_matrix[intersection.y -
                                    1, intersection.x + 1] -= 1
            neighbour_counts_matrix[intersection.y +
                                    1, intersection.x + 1] -= 1
            neighbour_counts_matrix[intersection.y, intersection.x] = 3
            digitized_image[intersection.y, intersection.x + 1] = 0


def compass_matrix_from_last_step(direction_vector: Vector[int]) -> list:
    direction_vector = direction_vector.unit_vector

    y = direction_vector.y
    x = direction_vector.x

    compass_matrix = [
        [-(y + x), -y, (x - y)],
        [-x, -6, x],
        [(y - x), y, (y + x)]
    ]
    return compass_matrix


def compass_matrix_from_last_step_5(direction_vector: Vector[int]) -> list:
    direction_vector = direction_vector.unit_vector

    y = direction_vector.y
    x = direction_vector.x

    compass_matrix = [
        [-2*(y+x), -2*y-x, -2*y, x-2*y, 2*(x - y)],
        [-(y + x), 0, 0, 0, (x - y)],
        [-2*x, 0, -6, 0, 2*x],
        [(y - x), 0, 0, 0, (y + x)],
        [2*(y-x), 2*y-x, 2*y, x+2*y, 2*(x + y)]
    ]
    return compass_matrix
