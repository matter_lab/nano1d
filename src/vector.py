import json
import math
from typing import TypeVar, Generic


T = TypeVar("T")


class Vector(Generic[T]):

    """Creates a vector on a coordinate plane with values x and y."""

    def __init__(
            self,
            x: int = None,
            y: int = None,
            NN: int = None,
            scan_count: int = 0
            ):
        """Defines x and y variables"""
        self.x = x
        self.y = y
        self.NN = NN
        self.scan_count = scan_count

    @staticmethod
    def yx_from_tuple(coordinates):
        result = Vector(
            x=coordinates[1],
            y=coordinates[0],
            NN=coordinates[2]
        )
        return result

    def yx_to_list(self):
        result = [self.y, self.x]
        return result

    def add_vector(self, vector):
        self.x = self.x + vector.x
        self.y = self.y + vector.y
        return self

    def vector_by_adding_vector(self, vector):
        return self.__copy__().add_vector(vector)

    def subtract_vector(self, vector):
        self.x = self.x - vector.x
        self.y = self.y - vector.y
        return self

    def vector_by_subtracting_vector(self, vector):
        return self.__copy__().subtract_vector(vector)

    def scaled_by_factor(self, scale):
        self.x = self.x * scale
        self.y = self.y * scale
        return self

    def vector_by_scaling_vector(self, scale):
        return self.__copy__().scaled_by_factor(scale)

    @property
    def length(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)

    @property
    def unit_vector(self):
        if self.length == 0:
            return self
        return self.vector_by_scaling_vector(1 / self.length)

    def toJson(self):
        return json.dumps(self, default=lambda o: o.__dict__)

    def dot_product_with_vector(self, vector):
        result = 0
        denominator = self.length * vector.length
        if denominator != 0:
            result = (self.x * vector.x + self.y * vector.y) / denominator
        return result

    def __copy__(self):
        return Vector(x=self.x, y=self.y)

    def __repr__(self):
        return "{ x: " + str(self.x) + ", y: " + str(self.y) + " NN: " + str(self.NN) + "}"

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.x == other.x and self.y == other.y
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)
