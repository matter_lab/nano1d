import os
import subprocess
import yaml
import nano_1d
from PyQt6 import QtCore, QtGui, QtWidgets
from pathlib import Path
from src.About import Ui_Dialog as Form


class canvas(QtWidgets.QLabel):
    def __init__(self, parent):
        super().__init__(parent)
        self.pixmap = QtGui.QPixmap(1, 1)
        parent.setWidget(self)
        self.begin, self.destination = QtCore.QPoint(), QtCore.QPoint()

    def open_image_dialog(self):
        dialog = QtWidgets.QFileDialog()
        dialog.setFileMode(QtWidgets.QFileDialog.FileMode.ExistingFile)
        dialog.setNameFilter("Images (*.*)")
        dialog.setViewMode(QtWidgets.QFileDialog.ViewMode.List)
        if dialog.exec():
            self.filename = dialog.selectedFiles()[0]
            # Puts the image in pixmap
            self.pixmap = QtGui.QPixmap(self.filename)
            # Puts the pixmap in imageLabel
            self.setPixmap(self.pixmap)
            self.resize(self.pixmap.size())
            window.pickColorButton.setHidden(False)
            window.measureScaleBarButton.setHidden(False)
            window.cropButton.setHidden(False)
            window.runButton.setHidden(False)
            parent_folder = Path(self.filename).resolve().parents[0]
            window.folderNameLabel.setText(str(parent_folder))
            window.folder_name = str(parent_folder)

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.setPen(QtGui.QColor(52, 216, 235))
        painter.drawPixmap(QtCore.QPoint(), self.pixmap)

        if not self.begin.isNull() and not self.destination.isNull():
            rectangular = QtCore.QRect(self.begin, self.destination)
            painter.drawRect(rectangular.normalized())

    def mousePressEvent(self, event):
        if event.buttons() & QtCore.Qt.MouseButton.LeftButton:
            self.begin = event.pos()
            self.destination = self.begin
            if pick_color_button_status:
                image = self.pixmap.toImage()
                color = QtGui.QColor(image.pixel(self.begin)).getRgb()[0]
                update_color_entry(str(color))
            self.update()

    def mouseMoveEvent(self, event):
        if event.buttons():
            self.destination = event.pos()
            self.update()

    def mouseReleaseEvent(self, event):
        if event.button() & QtCore.Qt.MouseButton.LeftButton:
            rectangular = QtCore.QRect(self.begin, self.destination)
            painter = QtGui.QPainter(self.pixmap)
            painter.setPen(QtGui.QColor(52, 235, 110))
            painter.drawRect(rectangular.normalized())
            scale_bar_length = abs((self.destination-self.begin).x())
            crop_height = abs((self.destination-self.begin).y())
            self.begin, self.destination = QtCore.QPoint(), QtCore.QPoint()
            if scale_button_status:
                update_scale_pixel_entry(str(scale_bar_length))
            elif crop_button_status:
                update_crop_entry(str(crop_height))
        elif event.button() & QtCore.Qt.MouseButton.RightButton:
            # Clears the rectangular
            self.pixmap = QtGui.QPixmap(self.filename)
            self.setPixmap(self.pixmap)
        self.update()


class Ui_MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.folder_name = './'

    def open_folder_dialog(self):
        dialog = QtWidgets.QFileDialog()
        dialog.setFileMode(QtWidgets.QFileDialog.FileMode.Directory)
        dialog.setViewMode(QtWidgets.QFileDialog.ViewMode.List)
        if dialog.exec():
            self.folder_name = dialog.selectedFiles()[0]
            self.runButton.setHidden(False)
            self.folderNameLabel.setText(f"{self.folder_name}")

    def execute(self):
        with open('config.yaml', 'r') as conig_file:
            config = yaml.safe_load(conig_file)
        config.update({
            'scale_bar': int(self.scaleEntry.text()),
            'scale_bar_px': int(self.scalePixelEntry.text()),
            'BRIGHTNESS_THRESHOLD': int(self.brightnessEntry.text()),
            'images_directory_name': self.folder_name,
            'CROP_HEIGHT': int(self.cropEntry.text())
        })
        nano_1d.Run(config).execute()

    def lock_unlock_pixel(self):
        if self.scalePixelEntry.isReadOnly():
            self.scalePixelEntry.setReadOnly(False)
        elif not self.scalePixelEntry.isReadOnly():
            self.scalePixelEntry.setReadOnly(True)

    def lock_unlock_crop(self):
        if self.cropEntry.isReadOnly():
            self.cropEntry.setReadOnly(False)
        elif not self.cropEntry.isReadOnly():
            self.cropEntry.setReadOnly(True)

    def setupUi(self, MainWindow):
        with open('config.yaml', 'r') as conig_file:
            config = yaml.safe_load(conig_file)
        font_9 = QtGui.QFont()
        font_9.setFamily("Segoe UI")
        font_9.setPointSize(9)
        font_10 = QtGui.QFont()
        font_10.setFamily("Arial Rounded MT Bold")
        font_10.setPointSize(10)
        font_11 = QtGui.QFont()
        font_11.setFamily("Arial Rounded MT Bold")
        font_11.setPointSize(11)
        font_12 = QtGui.QFont()
        font_12.setFamily("Arial Rounded MT Bold")
        font_12.setPointSize(12)

        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(960, 540)

        self.openFileAction = QtGui.QAction(MainWindow)
        self.openFileAction.setObjectName("Open Image")
        self.openFolderAction = QtGui.QAction(MainWindow)
        self.openFolderAction.setObjectName("Open Folder")
        self.openConfigAction = QtGui.QAction(MainWindow)
        self.openConfigAction.setObjectName("Soften")
        self.cropAction = QtGui.QAction(MainWindow)
        self.openConfigAction.setObjectName("Crop")
        self.sigmaAction = QtGui.QAction(MainWindow)
        self.sigmaAction.setObjectName("Sigma")
        self.searchDistanceAction = QtGui.QAction(MainWindow)
        self.searchDistanceAction.setObjectName("Search Distance")
        self.findTrisectionsAction = QtGui.QAction(MainWindow)
        self.findTrisectionsAction.setObjectName("Find Trisections")
        self.saveSkeletonized = QtGui.QAction(MainWindow)
        self.saveSkeletonized.setObjectName(
            "Save skeletonized image automatically")
        self.manualAction = QtGui.QAction(MainWindow)
        self.manualAction.setObjectName("Manual")
        self.aboutAction = QtGui.QAction(MainWindow)
        self.aboutAction.setObjectName("About")

        MainWindow.setFont(font_12)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("logo.png"),
                       QtGui.QIcon.Mode.Normal, QtGui.QIcon.State.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setAutoFillBackground(False)
        MainWindow.setStyleSheet("background-color: rgb(45, 69, 113);")
        MainWindow.setMouseTracking(True)

        self.centralwidget = QtWidgets.QWidget(parent=MainWindow)
        self.centralwidget.setFont(font_12)
        self.centralwidget.setAutoFillBackground(False)
        self.centralwidget.setStyleSheet("")
        self.centralwidget.setObjectName("centralwidget")

        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setContentsMargins(0, 9, 0, 0)
        self.horizontalLayout.setSpacing(1)
        self.horizontalLayout.setObjectName("horizontalLayout")

        self.leftFrame = QtWidgets.QFrame(parent=self.centralwidget)
        self.leftFrame.setMinimumSize(QtCore.QSize(0, 75))
        self.leftFrame.setMaximumSize(QtCore.QSize(75, 16777215))
        self.leftFrame.setAutoFillBackground(False)
        self.leftFrame.setStyleSheet("background-color: rgb(45, 69, 113);")
        self.leftFrame.setLineWidth(0)
        self.leftFrame.setObjectName("leftFrame")

        self.verticalLayout = QtWidgets.QVBoxLayout(self.leftFrame)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setSpacing(6)
        self.verticalLayout.setObjectName("verticalLayout")

        self.openImageButton = QtWidgets.QCommandLinkButton(
            parent=self.leftFrame)
        self.openImageButton.setMinimumSize(QtCore.QSize(65, 65))
        self.openImageButton.setMaximumSize(QtCore.QSize(65, 65))
        self.openImageButton.setAutoFillBackground(False)
        self.openImageButton.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("icons/open-photo.png"),
                        QtGui.QIcon.Mode.Normal, QtGui.QIcon.State.Off)
        self.openImageButton.setIcon(icon1)
        self.openImageButton.setIconSize(QtCore.QSize(48, 48))
        self.openImageButton.setObjectName("openImageButton")
        self.verticalLayout.addWidget(
            self.openImageButton, 0, QtCore.Qt.AlignmentFlag.AlignHCenter)

        self.openFolderButton = QtWidgets.QCommandLinkButton(
            parent=self.leftFrame)
        self.openFolderButton.setMinimumSize(QtCore.QSize(65, 65))
        self.openFolderButton.setMaximumSize(QtCore.QSize(65, 65))
        self.openFolderButton.setAutoFillBackground(False)
        self.openFolderButton.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("icons/open-folder.png"),
                        QtGui.QIcon.Mode.Normal, QtGui.QIcon.State.Off)
        self.openFolderButton.setIcon(icon2)
        self.openFolderButton.setIconSize(QtCore.QSize(48, 48))
        self.openFolderButton.setObjectName("openFolderButton")
        self.verticalLayout.addWidget(
            self.openFolderButton, 0, QtCore.Qt.AlignmentFlag.AlignHCenter)
        self.openFolderButton.clicked.connect(self.open_folder_dialog)

        self.pickColorButton = QtWidgets.QCommandLinkButton(
            parent=self.leftFrame)
        self.pickColorButton.setMinimumSize(QtCore.QSize(65, 65))
        self.pickColorButton.setMaximumSize(QtCore.QSize(65, 65))
        self.pickColorButton.setAutoFillBackground(False)
        self.pickColorButton.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("icons/pick.png"),
                        QtGui.QIcon.Mode.Normal, QtGui.QIcon.State.Off)
        self.pickColorButton.setIcon(icon2)
        self.pickColorButton.setIconSize(QtCore.QSize(48, 48))
        self.pickColorButton.setObjectName("pickColorButton")
        self.verticalLayout.addWidget(
            self.pickColorButton, 0, QtCore.Qt.AlignmentFlag.AlignHCenter)
        self.pickColorButton.clicked.connect(change_dropper_button_status)
        self.pickColorButton.setHidden(True)

        self.measureScaleBarButton = QtWidgets.QCommandLinkButton(
            parent=self.leftFrame)
        self.measureScaleBarButton.setMaximumSize(QtCore.QSize(65, 65))
        self.measureScaleBarButton.setSizeIncrement(QtCore.QSize(65, 65))
        self.measureScaleBarButton.setAutoFillBackground(False)
        self.measureScaleBarButton.setText("")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap("icons/scale.png"),
                        QtGui.QIcon.Mode.Normal, QtGui.QIcon.State.Off)
        self.measureScaleBarButton.setIcon(icon3)
        self.measureScaleBarButton.setIconSize(QtCore.QSize(48, 48))
        self.measureScaleBarButton.setObjectName("measureScaleBarButton")
        self.verticalLayout.addWidget(
            self.measureScaleBarButton, 0, QtCore.Qt.AlignmentFlag.AlignHCenter)
        self.measureScaleBarButton.clicked.connect(change_scale_button_status)
        self.measureScaleBarButton.setHidden(True)

        self.cropButton = QtWidgets.QCommandLinkButton(parent=self.leftFrame)
        self.cropButton.setMaximumSize(QtCore.QSize(65, 65))
        self.cropButton.setSizeIncrement(QtCore.QSize(65, 65))
        self.cropButton.setAutoFillBackground(False)
        self.cropButton.setText("")
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap("icons/crop.png"),
                        QtGui.QIcon.Mode.Normal, QtGui.QIcon.State.Off)
        self.cropButton.setIcon(icon4)
        self.cropButton.setIconSize(QtCore.QSize(48, 48))
        self.cropButton.setObjectName("cropButton")
        self.verticalLayout.addWidget(
            self.cropButton, 0, QtCore.Qt.AlignmentFlag.AlignHCenter)
        self.cropButton.clicked.connect(change_crop_button_status)
        self.cropButton.setHidden(True)

        self.runButton = QtWidgets.QCommandLinkButton(parent=self.leftFrame)
        self.runButton.setMaximumSize(QtCore.QSize(65, 65))
        self.runButton.setSizeIncrement(QtCore.QSize(65, 65))
        self.runButton.setAutoFillBackground(False)
        self.runButton.setText("")
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap("icons/start.png"),
                        QtGui.QIcon.Mode.Normal, QtGui.QIcon.State.Off)
        self.runButton.setIcon(icon5)
        self.runButton.setIconSize(QtCore.QSize(48, 48))
        self.runButton.setObjectName("runButton")
        self.verticalLayout.addWidget(
            self.runButton, 0, QtCore.Qt.AlignmentFlag.AlignHCenter)
        self.runButton.clicked.connect(self.execute)
        self.runButton.setHidden(True)

        self.horizontalLayout.addWidget(self.leftFrame)

        self.middleFrame = QtWidgets.QScrollArea(parent=self.centralwidget)
        self.middleFrame.setEnabled(True)
        self.middleFrame.setMinimumSize(QtCore.QSize(768, 552))
        self.middleFrame.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.middleFrame.setStyleSheet(u"background-color: rgb(0, 85, 127);")
        self.horizontalLayout.addWidget(self.middleFrame)
        self.openImageButton.clicked.connect(
            canvas(parent=self.middleFrame).open_image_dialog)

        self.rightFrame = QtWidgets.QFrame(parent=self.centralwidget)
        self.rightFrame.setMinimumSize(QtCore.QSize(200, 0))
        self.rightFrame.setMaximumSize(QtCore.QSize(200, 16777215))
        self.rightFrame.setFont(font_12)
        self.rightFrame.setStyleSheet("background-color: rgb(45, 69, 113);")
        self.rightFrame.setFrameShape(QtWidgets.QFrame.Shape.NoFrame)
        self.rightFrame.setObjectName("rightFrame")

        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.rightFrame)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setSpacing(1)
        self.verticalLayout_3.setObjectName("verticalLayout_3")

        self.scaleBarFrame = QtWidgets.QFrame(parent=self.rightFrame)
        self.scaleBarFrame.setMaximumSize(QtCore.QSize(200, 130))
        self.scaleBarFrame.setCursor(
            QtGui.QCursor(QtCore.Qt.CursorShape.ArrowCursor))
        self.scaleBarFrame.setAutoFillBackground(False)
        self.scaleBarFrame.setFrameShape(QtWidgets.QFrame.Shape.NoFrame)
        self.scaleBarFrame.setFrameShadow(QtWidgets.QFrame.Shadow.Plain)
        self.scaleBarFrame.setObjectName("scaleBarFrame")

        self.ScaleBarLabel = QtWidgets.QLabel(parent=self.scaleBarFrame)
        self.ScaleBarLabel.setGeometry(QtCore.QRect(10, 10, 76, 19))
        self.ScaleBarLabel.setMaximumSize(QtCore.QSize(180, 30))
        self.ScaleBarLabel.setFont(font_12)
        self.ScaleBarLabel.setStyleSheet("color: rgb(130, 174, 255);")
        self.ScaleBarLabel.setObjectName("ScaleBarLabel")

        self.scaleEntry = QtWidgets.QLineEdit(parent=self.scaleBarFrame)
        self.scaleEntry.setGeometry(QtCore.QRect(40, 40, 70, 25))
        self.scaleEntry.setMaximumSize(QtCore.QSize(70, 25))
        self.scaleEntry.setFont(font_12)
        self.scaleEntry.setAutoFillBackground(False)
        self.scaleEntry.setStyleSheet("background-color: rgb(130, 174, 255);")
        self.scaleEntry.setFrame(False)
        self.scaleEntry.setClearButtonEnabled(True)
        self.scaleEntry.setObjectName("scaleEntry")
        self.scaleEntry.insert(str(config['scale_bar']))

        self.scalePixelEntry = QtWidgets.QLineEdit(parent=self.scaleBarFrame)
        self.scalePixelEntry.setGeometry(QtCore.QRect(40, 80, 70, 25))
        self.scalePixelEntry.setMaximumSize(QtCore.QSize(70, 25))
        self.scalePixelEntry.setFont(font_12)
        self.scalePixelEntry.setStyleSheet(
            "background-color: rgb(130, 174, 255);")
        self.scalePixelEntry.setFrame(False)
        self.scalePixelEntry.setClearButtonEnabled(True)
        self.scalePixelEntry.setObjectName("scalePixelEntry")
        self.scalePixelEntry.insert(str(config['scale_bar_px']))

        self.pixelLabel = QtWidgets.QLabel(parent=self.scaleBarFrame)
        self.pixelLabel.setGeometry(QtCore.QRect(120, 80, 76, 25))
        self.pixelLabel.setMaximumSize(QtCore.QSize(180, 25))
        self.pixelLabel.setFont(font_12)
        self.pixelLabel.setStyleSheet("color: rgb(130, 174, 255);")
        self.pixelLabel.setObjectName("pixelLabel")

        self.nmRadioButton = QtWidgets.QRadioButton(parent=self.scaleBarFrame)
        self.nmRadioButton.setGeometry(QtCore.QRect(120, 30, 71, 20))
        self.nmRadioButton.setFont(font_12)
        self.nmRadioButton.setStyleSheet("color: rgb(130, 174, 255);")
        self.nmRadioButton.setObjectName("nmRadioButton")

        self.micronRadioButton = QtWidgets.QRadioButton(
            parent=self.scaleBarFrame)
        self.micronRadioButton.setGeometry(QtCore.QRect(120, 55, 89, 20))
        self.micronRadioButton.setFont(font_11)
        self.micronRadioButton.setStyleSheet("color: rgb(130, 174, 255);")
        self.micronRadioButton.setChecked(True)
        self.micronRadioButton.setObjectName("micronRadioButton")

        self.scaleLockButton = QtWidgets.QCommandLinkButton(
            parent=self.scaleBarFrame)
        self.scaleLockButton.setGeometry(QtCore.QRect(0, 76, 35, 35))
        self.scaleLockButton.setText("")
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap("icons/lock.png"),
                        QtGui.QIcon.Mode.Normal, QtGui.QIcon.State.Off)
        self.scaleLockButton.setIcon(icon6)
        self.scaleLockButton.clicked.connect(self.lock_unlock_pixel)
        self.scaleLockButton.setObjectName("scaleLockButton")
        self.verticalLayout_3.addWidget(self.scaleBarFrame)

        self.brightnessFrame = QtWidgets.QFrame(parent=self.rightFrame)
        self.brightnessFrame.setMaximumSize(QtCore.QSize(200, 100))
        self.brightnessFrame.setFont(font_9)
        self.brightnessFrame.setAutoFillBackground(False)
        self.brightnessFrame.setFrameShape(QtWidgets.QFrame.Shape.NoFrame)
        self.brightnessFrame.setFrameShadow(QtWidgets.QFrame.Shadow.Plain)
        self.brightnessFrame.setObjectName("brightnessFrame")

        self.beightnessLabel = QtWidgets.QLabel(parent=self.brightnessFrame)
        self.beightnessLabel.setGeometry(QtCore.QRect(10, 10, 200, 19))
        self.beightnessLabel.setMinimumSize(QtCore.QSize(200, 30))
        self.beightnessLabel.setMaximumSize(QtCore.QSize(200, 30))
        self.beightnessLabel.setFont(font_12)
        self.beightnessLabel.setStyleSheet("color: rgb(130, 174, 255);")
        self.beightnessLabel.setFrameShadow(QtWidgets.QFrame.Shadow.Plain)
        self.beightnessLabel.setObjectName("beightnessLabel")

        self.brightnessEntry = QtWidgets.QLineEdit(parent=self.brightnessFrame)
        self.brightnessEntry.setEnabled(True)
        self.brightnessEntry.setGeometry(QtCore.QRect(40, 40, 75, 25))
        self.brightnessEntry.setMaximumSize(QtCore.QSize(75, 25))
        self.brightnessEntry.setFont(font_12)
        self.brightnessEntry.setStyleSheet(
            "background-color: rgb(130, 174, 255);")
        self.brightnessEntry.setFrame(False)
        self.brightnessEntry.setClearButtonEnabled(True)
        self.brightnessEntry.setObjectName("brightnessEntry")
        self.verticalLayout_3.addWidget(self.brightnessFrame)
        self.brightnessEntry.insert(str(config['BRIGHTNESS_THRESHOLD']))

        self.cropFrame = QtWidgets.QFrame(parent=self.rightFrame)
        self.cropFrame.setMaximumSize(QtCore.QSize(200, 100))
        self.cropFrame.setAutoFillBackground(False)
        self.cropFrame.setFrameShape(QtWidgets.QFrame.Shape.NoFrame)
        self.cropFrame.setFrameShadow(QtWidgets.QFrame.Shadow.Plain)
        self.cropFrame.setObjectName("cropFrame")

        self.cropLabel = QtWidgets.QLabel(parent=self.cropFrame)
        self.cropLabel.setGeometry(QtCore.QRect(10, 10, 143, 19))
        self.cropLabel.setMaximumSize(QtCore.QSize(180, 30))
        self.cropLabel.setFont(font_12)
        self.cropLabel.setAutoFillBackground(False)
        self.cropLabel.setStyleSheet("color: rgb(130, 174, 255);")
        self.cropLabel.setObjectName("cropLabel")

        self.cropEntry = QtWidgets.QLineEdit(parent=self.cropFrame)
        self.cropEntry.setGeometry(QtCore.QRect(40, 40, 75, 25))
        self.cropEntry.setMaximumSize(QtCore.QSize(75, 25))
        self.cropEntry.setFont(font_12)
        self.cropEntry.setStyleSheet("background-color: rgb(130, 174, 255);")
        self.cropEntry.setFrame(False)
        self.cropEntry.setClearButtonEnabled(True)
        self.cropEntry.setObjectName("cropEntry")
        self.cropEntry.insert(str(config['CROP_HEIGHT']))

        self.lockCropButton = QtWidgets.QCommandLinkButton(
            parent=self.cropFrame)
        self.lockCropButton.setGeometry(QtCore.QRect(0, 35, 35, 35))
        self.lockCropButton.setText("")
        self.lockCropButton.setIcon(icon6)
        self.lockCropButton.setObjectName("lockCropButton")
        self.lockCropButton.clicked.connect(self.lock_unlock_crop)

        self.verticalLayout_3.addWidget(self.cropFrame)
        self.folderFrame = QtWidgets.QFrame(parent=self.rightFrame)
        self.folderFrame.setMaximumSize(QtCore.QSize(200, 60))
        self.folderFrame.setFrameShape(QtWidgets.QFrame.Shape.NoFrame)
        self.folderFrame.setFrameShadow(QtWidgets.QFrame.Shadow.Plain)
        self.folderFrame.setObjectName("folderFrame")

        self.folderLabel = QtWidgets.QLabel(parent=self.folderFrame)
        self.folderLabel.setGeometry(QtCore.QRect(10, 15, 49, 16))
        self.folderLabel.setMinimumSize(QtCore.QSize(160, 15))
        self.folderLabel.setMaximumSize(QtCore.QSize(160, 15))
        self.folderLabel.setFont(font_12)
        self.folderLabel.setStyleSheet("color: rgb(130, 174, 255);")
        self.folderLabel.setObjectName("folderLabel")
        self.verticalLayout_3.addWidget(self.folderFrame)

        self.folderNameLabel = QtWidgets.QTextBrowser(parent=self.folderFrame)
        self.folderNameLabel.setGeometry(QtCore.QRect(10, 100, 49, 30))
        self.folderNameLabel.setMinimumSize(QtCore.QSize(160, 100))
        self.folderNameLabel.setMaximumSize(QtCore.QSize(160, 100))
        self.folderNameLabel.setFont(font_12)
        self.folderNameLabel.setStyleSheet("color: rgb(130, 174, 255);")
        self.folderNameLabel.setObjectName("folderNameLabel")
        self.folderNameLabel.setFrameShape(QtWidgets.QFrame.Shape.NoFrame)
        self.verticalLayout_3.addWidget(self.folderNameLabel)

        self.brightnessFrame.raise_()
        self.folderFrame.raise_()
        self.cropFrame.raise_()
        self.scaleBarFrame.raise_()
        self.horizontalLayout.addWidget(self.rightFrame)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(parent=MainWindow)
        self.menubar.setEnabled(True)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 960, 25))
        self.menubar.setFont(font_12)
        self.menubar.setAutoFillBackground(False)
        self.menubar.setStyleSheet("background-color: rgb(45, 69, 113);\n"
                                   "color: rgb(255, 255, 255);")
        self.menubar.setDefaultUp(False)
        self.menubar.setNativeMenuBar(False)
        self.menubar.setObjectName("menubar")

        self.menuFile = QtWidgets.QMenu(parent=self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuFile.setFont(font_10)
        self.menuFile.setAutoFillBackground(False)
        self.menuFile.setStyleSheet("background-color: rgb(45, 69, 113);\n"
                                    "color: rgb(255, 255, 255);")

        self.menuOptions = QtWidgets.QMenu(parent=self.menubar)
        self.menuOptions.setObjectName("menuOptions")
        self.menuOptions.setFont(font_10)
        self.menuOptions.setAutoFillBackground(False)
        self.menuOptions.setStyleSheet("background-color: rgb(45, 69, 113);\n"
                                       "color: rgb(255, 255, 255);")

        self.menuHelp = QtWidgets.QMenu(parent=self.menubar)
        self.menuHelp.setObjectName("menuHelp")
        self.menuHelp.setFont(font_10)

        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(parent=MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuOptions.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())

        self.menuFile.addAction(self.openFileAction)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.openFolderAction)

        self.menuOptions.addAction(self.openConfigAction)
        self.menuOptions.addSeparator()

        self.menuHelp.addAction(self.manualAction)
        self.menuHelp.addSeparator()
        self.menuHelp.addAction(self.aboutAction)

        self.openFileAction.triggered.connect(canvas.open_image_dialog)
        self.openFolderAction.triggered.connect(self.open_folder_dialog)
        self.openConfigAction.triggered.connect(open_config)
        self.manualAction.triggered.connect(open_manual)
        self.aboutAction.triggered.connect(open_about)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Nano1D"))

        self.ScaleBarLabel.setText(_translate("MainWindow", "Scale Bar"))
        self.pixelLabel.setText(_translate("MainWindow", "Pixel"))
        self.nmRadioButton.setText(_translate("MainWindow", "nm"))
        self.micronRadioButton.setText(_translate("MainWindow", "µm"))
        self.beightnessLabel.setText(_translate(
            "MainWindow", "Brightness threshold"))
        self.cropLabel.setText(_translate("MainWindow", "Crop height (pixel)"))
        self.folderLabel.setText(_translate("MainWindow", "Folder"))

        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.menuOptions.setTitle(_translate("MainWindow", "Options"))
        self.menuHelp.setTitle(_translate("MainWindow", "Help"))
        self.openFileAction.setText(_translate("MainWindow", "Open File ..."))
        self.openFolderAction.setText(
            _translate("MainWindow", "Open Folder ..."))
        self.openConfigAction.setText(_translate("MainWindow", "Preferences"))
        self.manualAction.setText(_translate("MainWindow", "Manual"))
        self.aboutAction.setText(_translate("MainWindow", "About"))


def update_scale_pixel_entry(text):
    window.scalePixelEntry.clear()
    window.scalePixelEntry.insert(text)


def update_crop_entry(text):
    window.cropEntry.clear()
    window.cropEntry.insert(text)


def update_color_entry(text):
    window.brightnessEntry.clear()
    window.brightnessEntry.insert(text)


scale_button_status = False
crop_button_status = False
pick_color_button_status = False


def change_scale_button_status():
    global scale_button_status, crop_button_status, pick_color_button_status
    if scale_button_status == True:
        scale_button_status = False
        window.measureScaleBarButton.setStyleSheet("")
    elif scale_button_status == False:
        scale_button_status = True
        window.measureScaleBarButton.setStyleSheet("QCommandLinkButton"
                                                   "{"
                                                   "border : 3px solid black;"
                                                   "background-color : lightgreen;"
                                                   "}")
        crop_button_status = False
        pick_color_button_status = False
        window.cropButton.setStyleSheet("")
        window.pickColorButton.setStyleSheet("")


def change_crop_button_status():
    global crop_button_status, scale_button_status, pick_color_button_status
    if crop_button_status == True:
        crop_button_status = False
        window.cropButton.setStyleSheet("")
    elif crop_button_status == False:
        crop_button_status = True
        window.cropButton.setStyleSheet("QCommandLinkButton"
                                        "{"
                                        "border : 3px solid black;"
                                        "background-color : lightgreen;"
                                        "}")
        scale_button_status = False
        pick_color_button_status = False
        window.measureScaleBarButton.setStyleSheet("")
        window.pickColorButton.setStyleSheet("")


def change_dropper_button_status():
    global crop_button_status, scale_button_status, pick_color_button_status
    if pick_color_button_status == True:
        pick_color_button_status = False
        window.pickColorButton.setStyleSheet("")
    elif pick_color_button_status == False:
        pick_color_button_status = True
        window.pickColorButton.setStyleSheet("QCommandLinkButton"
                                             "{"
                                             "border : 3px solid black;"
                                             "background-color : lightgreen;"
                                             "}")
        scale_button_status = False
        crop_button_status = False
        window.measureScaleBarButton.setStyleSheet("")
        window.cropButton.setStyleSheet("")


def open_config():
    subprocess.Popen(["notepad.exe", 'config.yaml'])


def open_manual():
    os.startfile('Manual.pdf')


def open_about():
    dialog = QtWidgets.QDialog()
    dialog.ui = Form()
    dialog.ui.setupUi(dialog)
    dialog.exec()
    dialog.show()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    window = Ui_MainWindow()
    window.setupUi(window)
    window.show()
    sys.exit(app.exec())
