# Nano1D
Nano1D is a physics-based computational model for accurate segmentation and geometrical analysis of one-dimensional irregular and deformable objects from microscopy images. This model, named Nano1D, has four steps of preprocessing, segmentation, separating overlapped objects and geometrical measurements.  

# Support
For support, email ehsan.moradpur.tari@ut.ee.  

# License
[GNU GENERAL PUBLIC LICENSE](https://www.gnu.org/licenses/gpl-3.0.txt)  

# Installation
## 1- Python version
Install Nano1D with git:  
$ git clone https://gitlab.com/matter_lab/nano1d  

Python version can be run from python function:  
nano_1d.py  
or its Graphical User Interface file:  
nano_1d_GUI.py  

## 2- Executable version
Download the program from the link below, extract it and run.  
[Nano1D](https://www.dropbox.com/scl/fi/d0ef6sf3zy3enyf8bqe2a/Nano1D_Portable_v1.0.1.zip?rlkey=8ivszj0ndftzs6yox11r0s1q3&st=gue4zoeu&dl=0)  

# Tutorial
A video tutorial on how to use the code:
https://youtu.be/QjDpbbfDAZg?si=xkrbD-S-_W5bRbGN
 
    
# Citing
Nano1D is an open-source and freely available code. The details about its algorithms are  in
https://doi.org/10.1016/j.ultramic.2024.113949  

When publishing results obtained with the help of Nano1D, please cite:  
https://doi.org/10.1016/j.ultramic.2024.113949  
