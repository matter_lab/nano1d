from pathlib import Path
import time
from src.nano_1d_functions import *
from src.vector import Vector
from src.IO import *
import os
import statistics


class Run:
    def __init__(self, config: dict) -> None:
        (
            scale_bar,
            scale_bar_px,
            self.brightness_threshold,
            self.parent_folder,
            self.overwrite_results,
            self.pre_soften,
            self.crop_image,
            self.crop_height,
            self.sigma,
            self.skeletonization_method,
            self.remove_NN4,
            self.search_distance,
            self.remove_shared_tails,
            self.write_png,
            self.save_skeletonized_image,
            self.save_neighbor_number_image,
        ) = config.values()
        self.scale_bar = scale_bar/scale_bar_px

    def execute(self):
        startTime = time.time()
        folder_names = next(os.walk(self.parent_folder))[1]
        # Exclude dot (hidden) folders:
        folder_names = list(filter(lambda name: name[0] != ".", folder_names))
        if len(folder_names) == 0:
            folder_names = [self.parent_folder]
        print(f"""
Parent folder:
{self.parent_folder}

{len(folder_names)} folder(s) found.
""")

        for folder_name in folder_names:
            objects_length_images = []
            analysis_folder_name = f'{folder_name}_analysed'
            if not os.path.exists(analysis_folder_name):
                os.makedirs(analysis_folder_name)
            sem_image_collection = skimage.io.imread_collection(
                f'{folder_name}/*.*'
            )
            image_names_in_folder = sem_image_collection.files
            number_of_images = len(sem_image_collection)

            print(f"""
{number_of_images} image(s) were read from folder {folder_name}.

{40*"="}'
""")

            path_to_txt_output_file: Path = Path(os.path.join(
                f'{analysis_folder_name}', f'Measurement_Data.txt'))

            if not path_to_txt_output_file.is_file():
                with open(path_to_txt_output_file, 'w+') as output:
                    output.write(
                        f'Folder Name\tFile Name\tNo. of Objects\tAverage Length\tAverage Thickness\tDescription')
                    output.write(f'\n\t\t\tµm\tnm\n\n')

            print(f'\nFile\tNo.\tL_Avg\tA_Avg\td_Avg')

            for image_file_path in image_names_in_folder:
                image_file_name: str = os.path.basename(image_file_path)
                if not os.path.isfile(
                    f'{image_file_path}.PNG'
                ) or self.overwrite_results:

                    # Read the input image, crop it, remove small dots and skeletonize Image:
                    [
                        image,
                        binary_image,
                        skeletonized_image_binary,
                        self.brightness_threshold,
                    ] = Image(
                        self.brightness_threshold,
                        self.skeletonization_method,
                    ).prepare(
                        image_file_path,
                        self.pre_soften,
                        self.crop_image,
                        self.crop_height,
                        self.sigma,
                        self.save_skeletonized_image
                    )

                    # Find tails and intersections:
                    image_size = numpy.shape(image)
                    neighbour_number_image: ndarray[int] = image_of_neighbour_counts(
                        skeletonized_image_binary)
                    tail_points = points_by_value(
                        1, neighbour_number_image)
                    four_way_intersection_points = points_by_value(
                        4, neighbour_number_image)

                    # Remove center points of four way intersections:
                    if self.remove_NN4:
                        remove_four_way_intersection_points(
                            four_way_intersection_points,
                            neighbour_number_image,
                            skeletonized_image_binary
                        )
                    if self.save_neighbor_number_image:
                        pyplot.imsave(
                            os.path.join(
                                f'{folder_name}_analysed',
                                f'{image_file_name}_neighbour_number.PNG',
                            ),
                            neighbour_number_image,
                            cmap='hot',
                            dpi=1200,
                        )
                    skeletonized_lines: list[list[Vector[int]]] = [
                        [] for i in range(len(tail_points))
                    ]

                    # Scan and save all instances
                    for tail_index, first_tail in enumerate(tail_points):
                        # First step
                        if first_tail.scan_count == 0:
                            current_point = first_tail
                            current_line = skeletonized_lines[tail_index]
                            current_line.append(current_point)
                            [current_point, last_step_vector] = NextPoint(
                                current_point,
                                skeletonized_image_binary,
                            ).from_tail()
                            # Next steps
                            timeout = time.time() + 1   # 2 seconds from now
                            while True:
                                # section
                                if neighbour_number_image[current_point.y, current_point.x] == 2:
                                    current_point.NN = 2
                                    current_line.append(current_point)
                                    [
                                        current_point,
                                        last_step_vector,
                                    ] = NextPoint(
                                        current_point,
                                        skeletonized_image_binary,
                                    ).from_section(last_step_vector)
                                # intersection
                                if neighbour_number_image[
                                    current_point.y,
                                    current_point.x,
                                ] > 2:
                                    current_point.NN = 3
                                    current_line.append(current_point)
                                    [
                                        current_point,
                                        last_step_vector,
                                    ] = NextPoint(
                                        current_point,
                                        skeletonized_image_binary,
                                    ).from_intersection(
                                        current_line,
                                        self.search_distance,
                                        neighbour_number_image,
                                    )

                                # tail
                                if time.time() > timeout:
                                    print(f'timeout')
                                    break
                                if neighbour_number_image[
                                    current_point.y,
                                    current_point.x,
                                ] == 1:
                                    current_point.NN = 1
                                    current_point.scan_count += 1
                                    end_tail = next(
                                        (
                                            x for x in tail_points if x.x == current_point.x and x.y == current_point.y
                                        ), None)
                                    if end_tail != None:
                                        end_tail.scan_count += 1
                                        current_line.append(current_point)
                                    break

                    # Remove possible empty values from list
                    skeletonized_lines = list(filter(None, skeletonized_lines))
                    if self.remove_shared_tails:
                        remove_shared_tails(
                            skeletonized_lines,
                            self.search_distance
                        )
                    if self.write_png:
                        create_analysed_image(
                            image_size,
                            skeletonized_lines,
                            image,
                            image_file_name,
                            analysis_folder_name,
                        )
                    [
                        objects_length,
                        sum_length_of_objects,
                    ] = objects_length_image(
                        analysis_folder_name,
                        image_file_name,
                        skeletonized_lines,
                        self.scale_bar,
                    )
                    objects_length_images.append(objects_length)
                    number_of_objects = len(skeletonized_lines)
                    surface_area_of_objects = binary_image.sum(
                    ) * self.scale_bar**2
                    average_thickness = surface_area_of_objects / sum_length_of_objects
                    print(
                        f'{image_file_name}\t{number_of_objects}\t{sum_length_of_objects / number_of_objects:.2f}\t{int(1000 * average_thickness)}'
                    )
                    with open(path_to_txt_output_file, 'a+') as output:
                        output.write(
                            f'{folder_name}\t{image_file_name[-7:-4]}\t{number_of_objects}\t{sum_length_of_objects / number_of_objects:.2f}\t{int(1000 * average_thickness)}\tSkeletonization Method: {self.skeletonization_method}, Brightness Threshold: {self.brightness_threshold}\n'
                        )

            # write data for all images in the folder
            if len(objects_length_images) > 0:

                # flatten list of lists
                objects_length_images = [
                    x for xs in objects_length_images for x in xs
                ]

                with open(
                    os.path.join(
                        analysis_folder_name,
                        'Dimensions_of_objects_in_all_images.txt',
                    ),
                        'w',
                ) as folder_dimensions_file:
                    for object_length in objects_length_images:
                        folder_dimensions_file.write(f'{object_length:.2f}\n')
                try:
                    write_folder_statistics(
                        analysis_folder_name,
                        objects_length_images,
                    )
                except statistics.StatisticsError:
                    continue
        print(f"""
Done
Calculation time:
{time.time() - startTime}
The results are saved in {analysis_folder_name} folder
""")


if __name__ == '__main__':
    import yaml
    with open('config.yaml', 'r') as conig_file:
        config = yaml.safe_load(conig_file)
    Run(config).execute()
